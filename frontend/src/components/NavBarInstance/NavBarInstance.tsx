import React from 'react';
import Navbar from 'rsuite/Navbar';
import Nav from 'rsuite/Nav';
import Dropdown from 'rsuite/Dropdown';

interface NavBarInstanceProps {
    onSelect: any,
    activeKey: string | null,
    appearance: any

}

const NavBarInstance = ({ onSelect, activeKey, appearance, ...props }: NavBarInstanceProps) => {
    return (
        <Navbar appearance={appearance} {...props}>
            <Navbar.Brand >RSUITE</Navbar.Brand>
            <Nav onSelect={onSelect} activeKey={activeKey}>
                <Nav.Item eventKey="1">
                    Home
                </Nav.Item>
                <Nav.Item eventKey="2">News</Nav.Item>
                <Nav.Item eventKey="3">Products</Nav.Item>
                <Dropdown title="About">
                    <Dropdown.Item eventKey="4">Company</Dropdown.Item>
                    <Dropdown.Item eventKey="5">Team</Dropdown.Item>
                    <Dropdown.Item eventKey="6">Contact</Dropdown.Item>
                </Dropdown>
            </Nav>
            <Nav pullRight>
                <Nav.Item>Settings</Nav.Item>
            </Nav>
        </Navbar>
    );
};
export default NavBarInstance;
