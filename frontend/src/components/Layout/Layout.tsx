import { Button, ButtonToolbar, Container, Content, CustomProvider, FlexboxGrid, Footer, Form, Header, Navbar, Panel } from 'rsuite';
import styled from 'styled-components';
import '../../pages/App/App.css';

interface LayoutProps {
    children: JSX.Element,
    theme?: {
        theme?: "light" | "dark" | "high-contrast" | undefined
    },
}

const ContentWrapper = styled.div`
    margin: auto;
    padding: 2em 1em;
    height: 100%;
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
`;

const Layout = (props: LayoutProps) => {
    const parseTheme = () => (props.theme && props.theme.theme) || 'light';
    return (
        <Container>
            <Content>
                <FlexboxGrid justify="center">
                    <FlexboxGrid.Item colspan={18}>
                        <CustomProvider theme={parseTheme()}>
                            <ContentWrapper id="Content-wrapper">
                                {props.children}
                            </ContentWrapper>
                        </CustomProvider>
                    </FlexboxGrid.Item>
                </FlexboxGrid>
            </Content>
        </Container>
    );
}

export default Layout;

