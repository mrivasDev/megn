import { useEffect, useState } from 'react';
import styled from 'styled-components';
import Form from 'rsuite/Form';
import Modal from 'rsuite/Modal';
import Button from 'rsuite/Button';
import { Friend, FriendMutation, Gender } from '../../interfaces/friends';
import { SelectPicker } from 'rsuite';

const StyledButton = styled(Button)`
    display: inline-block;
    width: 100%;
    color: #fff;
    background-color: #000;
    padding: 0.8rem 1rem 0.7rem;
    border: 0.2rem solid #4d4d4d;
    cursor: pointer;
    text-transform: capitalize;
    overflow: visible;
    font-family: inherit;
    font-size: 100%;
    line-height: 1.15;
`;

type NewFriendsProps = {
    friendToEdit: Friend;
    onAdd: (newFriend: FriendMutation) => void;
};

const genderValues = [
    { label: 'Male', value: Gender.MALE },
    { label: 'Female', value: Gender.FEMALE },
    { label: 'Other', value: Gender.OTHER },
];

export const NO_FRIEND = {
    firstName: '',
    lastName: '',
    email: '',
    age: 0,
    gender: Gender.MALE,
    language: '',
};

const NewFriends = (props: NewFriendsProps) => {
    const [open, setOpen] = useState(false);
    const [formValue, setFormValue] = useState(NO_FRIEND);

    const handleClose = () => {
        setOpen(false);
    };
    const handleOpen = () => {
        setOpen(true);
    };

    const addFriend = () => {
        props.onAdd(formValue);
        setFormValue(NO_FRIEND)
        handleClose();
    }

    useEffect(() => {
        console.log('Changed');
        console.log({friend: props.friendToEdit})
        if (props.friendToEdit.id) {
            handleOpen();
        }
    }, [props.friendToEdit]);

    return (
        <div>
            <Modal open={open} onClose={handleClose} size="xs">
                <Modal.Header>
                    <Modal.Title>New Friend</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form fluid onChange={(e) => { setFormValue(e as any) }} formValue={formValue}>
                        <Form.Group controlId="firstName-9">
                            <Form.ControlLabel htmlFor="firstName">First</Form.ControlLabel>
                            <Form.Control id="firstName" name="firstName" type="text" autoComplete="off" />
                        </Form.Group>
                        <Form.Group controlId="lastName-9">
                            <Form.ControlLabel htmlFor="lastName">Last</Form.ControlLabel>
                            <Form.Control id="lastName" name="lastName" type="text" autoComplete="off" />
                        </Form.Group>
                        <Form.Group controlId="email-9">
                            <Form.ControlLabel htmlFor="email">Email</Form.ControlLabel>
                            <Form.Control id="email" name="email" type="email" autoComplete="off" />
                        </Form.Group>
                        <Form.Group controlId="age-9">
                            <Form.ControlLabel htmlFor="age">Age</Form.ControlLabel>
                            <Form.Control id="age" name="age" type="number" autoComplete="off" />
                        </Form.Group>
                        <Form.Group controlId="language-9">
                            <Form.ControlLabel htmlFor="language">Language</Form.ControlLabel>
                            <Form.Control id="language" name="language" type="text" autoComplete="off" />
                        </Form.Group>
                        <Form.Group controlId="gender-9">
                            <Form.ControlLabel htmlFor="gender">Gender</Form.ControlLabel>
                            <Form.Control id="gender" name="gender" searchable={false} style={{ width: 224 }} accepter={SelectPicker} data={genderValues} />
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={addFriend} appearance="primary">
                        Add friend
                    </Button>
                    <Button onClick={handleClose} appearance="subtle">
                        Cancel
                    </Button>
                </Modal.Footer>
            </Modal>
            <StyledButton onClick={handleOpen} block appearance="primary">Add new friend</StyledButton>
        </div>
    );
}

export default NewFriends;

