import React from 'react';
import styled from 'styled-components';
import { Friend } from '../../interfaces/friends';

const ErrorMessage = styled.p`
  color: palevioletred;
`;

type ErrorType = {
    id: number,
    message: string
}

type FriendsErrorProps = {
    errorList: ErrorType[];
};

const FriendsError = (props: FriendsErrorProps) => {
    return (
        <div>
            {props.errorList?.map((error: ErrorType) => (
                <ErrorMessage key={error.id}>{error.message}</ErrorMessage>
            ))}
        </div>
    );
}

export default FriendsError;

