import React, { Dispatch } from 'react';
import ButtonGroup from 'rsuite/ButtonGroup';
import IconButton from 'rsuite/IconButton';
import { FaMoon, FaSun } from 'react-icons/fa';


import { connect } from 'react-redux';
import { setTheme } from '../../core/actionCreators/themeActionCreators';
import { themeAction } from '../../core/actionTypes/themeActionTypes';

interface ThemeChangerProps {
    setTheme?: (theme: "light" | "dark" | "high-contrast" | undefined) => void
}

const themeChanger = ({ setTheme }: ThemeChangerProps) => {
    return (
        <div>
            <ButtonGroup>
                <IconButton onClick={() => setTheme && setTheme('light')} icon={<FaSun />} />
                <IconButton onClick={() => setTheme && setTheme('dark')} icon={<FaMoon />} />
            </ButtonGroup>
        </div>
    );
}

const mapDispatchToProps = (dispatch: Dispatch<themeAction>) => ({
    setTheme: (theme: "light" | "dark" | "high-contrast" | undefined) => {
        dispatch(setTheme(theme));
    }
});

export default connect(
    null,
    mapDispatchToProps
)(themeChanger);