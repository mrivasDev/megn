import { cloneElement } from 'react';
import List from 'rsuite/List';
import Message from 'rsuite/Message';
import Placeholder from 'rsuite/Placeholder';
import FlexboxGrid from 'rsuite/FlexboxGrid';
import styled from 'styled-components';
import { FaUserAlt, FaUserEdit, FaUserTimes, FaMale, FaFemale } from 'react-icons/fa';
import { Friend } from '../../interfaces/friends';

const FriendsWrapper = styled.div`
    font-size: 20px;
    padding: 0.5em 1em;
`;

type FriendsListProps = {
  friends: Friend[];
  loading: boolean;
  onRemove: (friendId: string | undefined) => void;
  onEdit: (friend: Friend) => void;
};

const styleCenter = {
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  height: '60px',
};

const slimText = {
  fontSize: '1.2em',
  color: '#97969B',
  fontWeight: 'lighter',
  paddingBottom: 5,
};

const dataStyle = {
  fontSize: '1.2em',
  fontWeight: 500,
};

const alertImg = {
  width: '35px',
  heigth: 'auto'
}

const FriendsList = ({ friends, loading, onRemove, onEdit }: FriendsListProps) => {
  const RenderNoFriends: JSX.Element = (
    <Message showIcon type="info" header="For ever alone">
      <img style={alertImg}
        src={`${process.env.PUBLIC_URL}/assets/img/foreveralone.png`}
        alt="logo" />
      <div>Oops, it seems that you are alone on this one :(</div>
      <div>To add new friends just click on the button below</div>
    </Message>
  );

  const RenderLoading: JSX.Element = (
    <Placeholder.Paragraph style={{ marginTop: 30 }} />
  );

  const getGenderIcon = ({ gender }: Friend): JSX.Element => {
    switch (gender) {
      case 'MALE': return <FaMale />; break;
      case 'FEMALE': return <FaFemale />; break;
      default: return <FaUserAlt />; break;
    }
  }

  const RenderFriendsList: JSX.Element = (
    <List hover>
      {friends.map((friend: Friend, index: number) => (
        <List.Item key={`${friend.id}`} index={index + 1}>
          <FlexboxGrid>
            <FlexboxGrid.Item colspan={2} style={styleCenter}>
              {cloneElement(getGenderIcon(friend), {
                style: {
                  color: 'darkgrey',
                  fontSize: '1.5em',
                },
              })}
            </FlexboxGrid.Item>
            <FlexboxGrid.Item
              colspan={6}
              style={{
                ...styleCenter,
                flexDirection: 'column',
                alignItems: 'flex-start',
                overflow: 'hidden',
              }}
            >
              <div>{`${friend.firstName} ${friend.lastName}`}</div>
            </FlexboxGrid.Item>
            <FlexboxGrid.Item colspan={6} style={styleCenter}>
              <div style={{ textAlign: 'right' }}>
                <div style={dataStyle}>
                  {friend.email || (<div style={slimText}>No email</div>)}
                </div>
              </div>
            </FlexboxGrid.Item>
            <FlexboxGrid.Item colspan={6} style={styleCenter}>
              <div style={{ textAlign: 'right' }}>
                <div style={dataStyle}>{friend.language}</div>
              </div>
            </FlexboxGrid.Item>
            <FlexboxGrid.Item
              colspan={4}
              style={{
                ...styleCenter,
              }}
            >
              {cloneElement(<FaUserEdit onClick={() => onEdit(friend)} />, {
                style: {
                  cursor: 'pointer',
                  color: 'darkgrey',
                  fontSize: '1.5em',
                },
              })}

              <span style={{ padding: 5 }}>|</span>
              {cloneElement(<FaUserTimes onClick={() => onRemove(friend?.id)} />, {
                style: {
                  cursor: 'pointer',
                  color: 'darkgrey',
                  fontSize: '1.5em',
                },
              })}

            </FlexboxGrid.Item>
          </FlexboxGrid>
        </List.Item>
      ))}
    </List>
  );

  return (
    <FriendsWrapper>
      {
        loading
          ? RenderLoading
          : friends
            && friends.length
            ? RenderFriendsList
            : RenderNoFriends
      }

    </ FriendsWrapper>
  );
}

export default FriendsList;

