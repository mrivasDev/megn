
import { gql, useQuery } from '@apollo/client';
import { Gender } from '../interfaces/friends';
export const FETCH_FRIENDS = gql`
query{
    getAllFriend {
        id,
        firstName,
        lastName,
        age,
        email,
        gender,
        language
  }
}
`;

export const CREATE_FRIEND = gql`
mutation CreateFriend(
    $firstName:String!,
    $lastName:String,
    $language:String,
    $email:String!,
    $age: Int,
    $gender: Gender
){
  createFriend(
    input: {
      firstName: $firstName,
      lastName: $lastName,
      language: $language,
      email: $email,
      age: $age,
      gender: $gender
    },
  ){
    id,
    firstName,
    lastName,
    gender,
    language,
    age,
    gender,
    email
  }
}
`;
export const UN_FRIEND = gql`
mutation removeFriend($id: ID!){
  removeFriend(
    input: { id: $id },
  ){
    message,
      success
  }
}
`;

