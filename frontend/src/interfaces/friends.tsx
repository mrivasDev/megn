export interface Contact {
    firstName: string,
    lastName: string
}

export enum Gender {
    MALE='MALE',
    FEMALE='FEMALE',
    OTHER='OTHER',
}

export interface Friend {
    id?: string,
    firstName: string,
    lastName: string
    gender: any,
    language: string,
    age: number,
    email: string,
    contacts?: [Contact]
}

export interface FriendMutation {
    firstName?: string,
    lastName?: string
    gender?: Gender,
    language?: string,
    age?: number,
    email?: string,
    contacts?: [Contact]
}

export interface FriendMutationFormValue {
    firstName?: string,
    lastName?: string
    gender?: Gender,
    language?: string,
    age?: string,
    email?: string,
    contacts?: [Contact]
}