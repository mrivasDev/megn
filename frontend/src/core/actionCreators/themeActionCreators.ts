import * as actions from "../actionTypes/themeActionTypes";

export function setTheme(theme: "light" | "dark" | "high-contrast" | undefined): actions.SetThemeAction {
    return {
        type: actions.SET_THEME,
        theme
    };
}

export function getTheme(): actions.GetThemeAction {
    return {
        type: actions.GET_THEME,
    };
}

export function getThemeRequest(): actions.GetThemeRequestAction {
    return {
        type: actions.GET_THEME_REQUEST
    };
}

export function getThemeSuccess(
    theme: "light" | "dark" | "high-contrast" | undefined
): actions.GetThemeSuccessAction {
    return {
        type: actions.GET_THEME_SUCCESS,
        theme
    };
}

export function getThemeFailure(
    error: Error | string
): actions.GetThemeFailureAction {
    return {
        type: actions.GET_THEME_FAILURE,
        error
    };
}
