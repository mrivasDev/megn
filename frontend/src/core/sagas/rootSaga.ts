import { all, fork } from "redux-saga/effects";
import ThemeSaga from "./themeSaga";

export default function* rootSaga() {
  yield all([fork(ThemeSaga)]);
}
