import { put, call, takeEvery, all, fork } from "redux-saga/effects";

import { fetchTheme } from "../services/themeService";
import * as actionCreators from "../actionCreators/themeActionCreators";
import * as actionTypes from "../actionTypes/themeActionTypes";

function* onLoadTheme() {
    try {
        yield put(actionCreators.getThemeRequest());
        const { data } = yield call(fetchTheme);
        yield put(actionCreators.getThemeSuccess(data.theme));
    } catch (error: any) {
        yield put(actionCreators.getThemeFailure(error?.response?.data?.error));
    }
}

function* onSetTheme(action: { type: string, theme: "light" | "dark" | "high-contrast" | undefined }) {
    try {
        localStorage.setItem('theme', action.theme || "light");

    } catch (error: any) {
        yield put(actionCreators.getThemeFailure(error?.response?.data?.error));
    }
}


function* watchOnLoadTheme() {
    yield takeEvery(actionTypes.GET_THEME, onLoadTheme);
    yield takeEvery(actionTypes.SET_THEME, onSetTheme);
}

export default function* themeSaga() {
    yield all([fork(watchOnLoadTheme)]);
}
