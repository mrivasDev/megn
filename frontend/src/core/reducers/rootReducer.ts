import { combineReducers } from "redux";
import themeReducer from "./themeReducer";
import isLoadingReducer from "./isLoadingReducer";
import errorReducer from "./errorReducer";

const rootReducer = combineReducers({
    theme: themeReducer,
    isLoading: isLoadingReducer,
    error: errorReducer
});

export type AppState = ReturnType<typeof rootReducer>;

export default rootReducer;
