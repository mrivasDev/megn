import * as actions from "../actionTypes/themeActionTypes";

export interface ThemeState {
    theme: "light" | "dark" | "high-contrast" | undefined;
}

const initialState: ThemeState = {
    theme: "light"
};

export default function themeReducer(
    state: ThemeState = initialState,
    action: actions.themeAction
): ThemeState {
    switch (action.type) {
        case actions.SET_THEME:
        case actions.GET_THEME_SUCCESS:
            return {
                theme: action.theme
            };
        default:
            return state;
    }
}
