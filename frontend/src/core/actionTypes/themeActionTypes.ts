export const SET_THEME = "themeActionTypes/SET_THEME";
export interface SetThemeAction {
    type: typeof SET_THEME;
    theme: "light" | "dark" | "high-contrast" | undefined;
}

export const GET_THEME = "themeActionTypes/GET_THEME";
export interface GetThemeAction {
    type: typeof GET_THEME;
}

export const GET_THEME_REQUEST = "themeActionTypes/GET_THEME_REQUEST";
export interface GetThemeRequestAction {
    type: typeof GET_THEME_REQUEST;
}

export const GET_THEME_SUCCESS = "themeActionTypes/GET_THEME_SUCCESS";
export interface GetThemeSuccessAction {
    type: typeof GET_THEME_SUCCESS;
    theme: "light" | "dark" | "high-contrast" | undefined;
}

export const GET_THEME_FAILURE = "themeActionTypes/GET_THEME_FAILURE";
export interface GetThemeFailureAction {
    type: typeof GET_THEME_FAILURE;
    error: Error | string;
}

export type themeAction =
    | SetThemeAction
    | GetThemeAction
    | GetThemeRequestAction
    | GetThemeSuccessAction
    | GetThemeFailureAction;
