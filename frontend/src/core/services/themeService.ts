import { getTheme } from '../../hooks/theme'

interface themeResponse {
    theme: "light" | "dark" | "high-contrast" | undefined;
}

export function fetchTheme(): themeResponse {
    return ({ theme: getTheme(localStorage.getItem('theme')) });
}
