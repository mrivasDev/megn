import { useEffect, useState } from 'react'

export const getTheme = (theme: string | undefined | null): "light" | "dark" | "high-contrast" | undefined => {
    switch (theme) {
        case 'light': return 'light';
        case 'dark': return 'dark';
        case 'high-contrast': return 'high-contrast';
        default:
            return undefined;
    }
}

const useTheme = () => {
    const [theme, setTheme] = useState<"light" | "dark" | "high-contrast" | undefined>(undefined);

    const toggleTheme = () => {
        if (theme === 'light') {
            window.localStorage.setItem('theme', 'dark')
            setTheme('dark')
        } else {
            window.localStorage.setItem('theme', 'light')
            setTheme('light')
        }
    }

    const darkTheme = () => { localStorage.setItem('theme', 'dark'); setTheme('dark') }

    const lightTheme = () => { localStorage.setItem('theme', 'light'); setTheme('light') }

    useEffect(() => {
        const themePicked = localStorage.getItem('theme');
        if (themePicked) {
            setTheme(getTheme(themePicked));
        } else {
            localStorage.setItem('theme', 'light');
        }
    }, []);

    useEffect(() => { console.log(theme) }, [theme])

    return { theme, toggleTheme, darkTheme, lightTheme };
}

export default useTheme;