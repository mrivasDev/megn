import React, { useEffect, useState } from 'react';
import { useMutation, useQuery } from '@apollo/client';
import FlexboxGrid from 'rsuite/FlexboxGrid';
import Row from 'rsuite/Row';
import styled from 'styled-components';
import FriendsList from '../../components/FriendsList/friendsList';
import NewFriends, { NO_FRIEND } from '../../components/NewFriends/newFriends';
import { CREATE_FRIEND, FETCH_FRIENDS, UN_FRIEND } from '../../queries/friends';
import { Friend, FriendMutation, Gender } from '../../interfaces/friends';
import ThemeChanger from '../../components/ThemeChanger/themeChanger';

const FriendsWrapper = styled.div`
    box-shadow: 0 5px 30px rgb(0 0 0 / 20%);
    padding: 2em;
    width: 100%;
    height: 80%;
`;

const Friends = () => {
    const [friends, setFriends] = useState([]);
    const [friendToEdit, setFriendToEdit] = useState(NO_FRIEND);
    const [loadingState, setLoadingState] = useState(false);
    const { loading, error, data, refetch } = useQuery(FETCH_FRIENDS);
    const [createFriend, mutationResult] = useMutation(CREATE_FRIEND);
    const [removeFriend, removeFriendResult] = useMutation(UN_FRIEND);

    useEffect(() => {
        if (data && data.getAllFriend) {
            setFriends(data.getAllFriend);
        }
    }, [data]);

    useEffect(() => {
        setLoadingState(loading);
    }, [loading]);

    const sanitizeParams = ({
        age,
        email = '',
        firstName = 'No name',
        gender = Gender.OTHER,
        language = 'English',
        lastName = '',
    }: {
        age?: any,
        email?: string | undefined,
        firstName?: string | undefined,
        gender?: Gender | undefined,
        language?: string | undefined,
        lastName?: string | undefined,
    }): FriendMutation => {
        return {
            age: (age && parseInt(age)) || 0,
            email: email || 'noEmail',
            firstName,
            gender: gender || Gender.OTHER,
            language: language || 'English',
            lastName: lastName || '',
        }
    }

    const unFriend = (friendId: string | undefined) => {
        removeFriend({ variables: { id: friendId } });
        refetch();
    }

    const addNewFriend = (newFriend: FriendMutation) => {
        createFriend({ variables: sanitizeParams(newFriend) });
        refetch();
    }

    const handleEdit = (friend: Friend) => {
        console.log('handle edit');
        setFriendToEdit(friend);
    }

    return (
        <FriendsWrapper>
            <FlexboxGrid style={{ height: "100%" }} justify="space-between" align="middle">
                <FlexboxGrid.Item style={{
                    marginTop: "0",
                    marginBottom: "auto",
                    marginLeft: "auto",
                    marginRight: "auto",
                }} as={Row} colspan={0} md={2}>
                    <ThemeChanger />
                </FlexboxGrid.Item>
                <FlexboxGrid.Item as={Row} colspan={24} md={8}>
                    <FriendsList onEdit={handleEdit} onRemove={unFriend} loading={loadingState} friends={friends} />
                </FlexboxGrid.Item>
                <FlexboxGrid.Item style={{ marginTop: "auto", width: "100%" }} as={Row} colspan={0} md={2}>
                    <NewFriends friendToEdit={friendToEdit} onAdd={addNewFriend} />
                </FlexboxGrid.Item>
            </FlexboxGrid>
        </FriendsWrapper>
    );
}

export default Friends;