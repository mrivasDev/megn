import { connect } from "react-redux";
import * as actionTypes from "../../core/actionTypes/themeActionTypes";
import Layout from '../../components/Layout/Layout';
import Friends from "../Friends/friends";

interface AppProps {
  theme?: {
    theme?: "light" | "dark" | "high-contrast" | undefined
  },
}

const App = ({ theme }: AppProps) => {
  return (
    <div className="App">
      <Layout theme={theme}>
        <Friends />
      </Layout>
    </div >
  );
}

const mapStateToProps = (state: any) => {
  return {
    theme: state.theme,
    isLoading: state.isLoading[actionTypes.GET_THEME],
    error: state.error[actionTypes.GET_THEME]
  };
}

export default connect(
  mapStateToProps,
)(App);
